(define add(lambda (m n s z)( m s (n s z))))

(define subtract(lambda (m n)( m pred n)))

(define and(lambda (m n)( m n false)))

(define or(lambda (m n)( m true n)))

(define not(lambda (m)( m false true)))

(define LEQ(lambda (m n)( isZero(subtract m n))))

(define GEQ(lambda (m n)( or( and(LEQ m n)(LEQ n m))(not (LEQ m n)))))